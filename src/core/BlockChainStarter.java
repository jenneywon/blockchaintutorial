package core;

import util.Util;
import java.util.ArrayList;


public class BlockChainStarter {

	public static void main(String[] args) {
				
		Block block1 = new Block(1, null, 0, new ArrayList());
		block1.mine();
		block1.time();
		block1.getInfo();

		Block block2 = new Block(2, block1.getPreviousBlockHash(), 0, new ArrayList());
		block2.addTransaction(new Transaction("Jen", "Momo", 3.5));
		block2.addTransaction(new Transaction("Sana", "Mina", 1.1));
		block2.mine();
		block2.time();
		block2.getInfo();
		
		Block block3 = new Block(3, block2.getPreviousBlockHash(), 0, new ArrayList());
		block3.addTransaction(new Transaction("Mina", "Jen", 5.5));
		block3.addTransaction(new Transaction("Nayeon", "Jihyo", 10.1));
		block3.addTransaction(new Transaction("Jeongyeon", "Tsuyu", 18.8));
		block3.mine();
		block3.time();
		block3.getInfo();

		
	}
}


