package core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import util.Util;

public class Block {
	private int blockID;
	private String previousBlockHash;
	private int nonce;
	private ArrayList<Transaction> transactionList;
	
	//constructor
	public Block(int blockID, String previousBlockHash, int nonce, ArrayList transactionList) {
		super();
		this.blockID = blockID;
		this.previousBlockHash = previousBlockHash;
		this.nonce = nonce;
		this.transactionList = transactionList;
	}
	
	public void addTransaction(Transaction transaction) {
		transactionList.add(transaction);
	}
	
	//getter&setters
	public int getBlockID() {
		return blockID;
	}
	
	public void setBlockID(int blockID) {
		this.blockID = blockID;
	}
	
	public String getPreviousBlockHash() {
		return previousBlockHash;
	}

	public void setPreviousBlockHash(String previousBlockHash) {
		this.previousBlockHash = previousBlockHash;
	}

	public int getNonce() {
		return nonce;
	}
	
	public void setNonce(int nonce) {
		this.nonce = nonce;
	}
	
	public ArrayList transactionList() {
		return transactionList;
	}
	
	public void setTransactionList(ArrayList transactionList) {
		this.transactionList = transactionList;
	}
	
	//get hash value
	public String getBlockHash() {
		String transactionInfo = "";
		for(int i=0; i<transactionList.size(); i++) {
			transactionInfo += transactionList.get(i).getInfo();
		}
		return Util.getHash(nonce + transactionInfo + previousBlockHash);
	}
	
	//prints out all informations
	public void getInfo() {
		System.out.println("Block ID: " + getBlockID());
		System.out.println("Previous Hash: " + getPreviousBlockHash());
		System.out.println("Nonce value: " + getNonce());
		System.out.println("Number of transaction: " + transactionList.size());
		for(int i=0; i<transactionList.size(); i++) {
			System.out.println(transactionList.get(i).getInfo());
		}
		System.out.println("Hash: " + getBlockHash());
		System.out.println("-----------------------------------------------------------------------");
	}

	//mining how
	public void mine() {
		while(true) {
			if(getBlockHash().substring(0, 6).contentEquals("111111")) {
				System.out.println("Block " + blockID + " has mined successfully.");
				break;
			}
			nonce++;
		}
	}
	
	public void time() {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		System.out.println("Local time: " + timeStamp);
	}
	
}
